import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { environment } from '../../environments/environment'


@Injectable({
  providedIn: 'root'
})
export class ApiService {


  tag: boolean = false;
  options: boolean = true;
  changeToogle=new Subject();
  isSideBarCollapsed:boolean=false;
 
  constructor(private http: HttpClient) {

   }

  

  register = `https://test-durgesh.xyz/api/doctor/signup`
  otp=`${environment.API_URL}` + "/doctor/sendOtp";
  verifyUser=`${environment.API_URL}` + "/doctor/verifyUser"
  profile=`${environment.API_URL}` + "/doctor/buildProfile"

  signUp(data){
    console.log(this.register)
    return this.http.post<any>(this.register, data)
      .pipe(
        map((res: any) => this.response(res)),

        catchError(this.errorHandler))
  }

  sendOtp(data){
    return this.http.post<any>(this.otp, data)
      .pipe(
        map((res: any) => this.response(res)),

        catchError(this.errorHandler))
  }

  buildDataProfile(data){
    return this.http.post<any>(this.profile, data)
      .pipe(
        map((res: any) => this.response(res)),

        catchError(this.errorHandler))
  }

  verifyUser1(data){
    return this.http.post<any>(this.verifyUser, data)
      .pipe(
        map((res: any) => this.response(res)),

        catchError(this.errorHandler))
  }

  response(res) {
    if (res.status == 200)
      return res;
    else
      throw new Error(res.message);
  }
  errorHandler(error: HttpErrorResponse) {
    return throwError(error);
  }
  
}
