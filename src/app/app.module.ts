import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import {HeaderModule} from './modules/header/header.module';
import {AuthModule} from "./modules/auth/auth.module";
import { RouterModule } from '@angular/router';
import {AppRoutes} from './app-routing'
import {AppRoutingModule} from './app-routing.module'
import {RegisterComponent} from './modules/auth/register/register.component';

import {ApiService} from './services/api.service';
import Amplify, {Auth} from 'aws-amplify';

Amplify.configure({
  Auth: {
    mandatorySignIn: true,
    region: 'us-east-2',
    userPoolId: 'us-east-2_7sf2SHflo',
    userPoolWebClientId: '29ddff83iiup59rpjbgfeucd3n',
    authenticationFlowType: 'USER_PASSWORD_AUTh'
  }
})


@NgModule({
  declarations: [
    AppComponent,

    // LoginComponent,
    // HeaderComponent
  ],
  imports: [
    BrowserModule,
    HeaderModule,
    AuthModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
