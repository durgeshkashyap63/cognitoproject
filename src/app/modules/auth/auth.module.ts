import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from '../auth/login/login.component';
import {RegisterComponent} from '../auth/register/register.component'
import {ForgotPasswordComponent} from '../auth/forgot-password/forgot-password.component'
import { RouterModule, Routes } from '@angular/router';
import {AuthComponent} from './auth.component'
import {ReactiveFormsModule,FormsModule} from '@angular/forms';
import { OptverificationComponent } from "../auth/optverification/optverification.component";
import {SpecializationComponent} from "../auth/specialization/specialization.component"


@NgModule({
  declarations: [AuthComponent,LoginComponent , RegisterComponent,ForgotPasswordComponent,OptverificationComponent,SpecializationComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
        {path:'auth',component:AuthComponent, children: [
        {path: '', redirectTo: 'login', pathMatch: 'full'},
        {path:'login',component:LoginComponent},
        {path:'signup',component:RegisterComponent},
        {path:'forgotpassword',component:ForgotPasswordComponent},
        {path:'otpverification',component:OptverificationComponent},
        {path:'buildProfile',component:SpecializationComponent},
        ]}
          ]) ,
    ReactiveFormsModule,
  ],
  exports:[RouterModule
  ],
  providers:[]
})
export class AuthModule { }
