import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import {ApiService} from '../../../services/api.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-specialization',
  templateUrl: './specialization.component.html',
  styleUrls: ['./specialization.component.css']
})
export class SpecializationComponent implements OnInit {
  fullName:string
  profileForm:FormGroup
  name:AbstractControl
  specialization:AbstractControl
  city:AbstractControl
  
  
  constructor(private formBuilder: FormBuilder,private apiService:ApiService,public router:Router) { }

  ngOnInit() {
    this.fullName=localStorage.getItem('fullName')
    this.name = new FormControl('', [Validators.required, Validators.email]);
    this.specialization = new FormControl('', [Validators.required]);
    this.city = new FormControl('', [Validators.required]);
    this.profileForm = this.formBuilder.group({
      name: this.name,
      specialization:this.specialization,
      city:this.city,
    })
  }

  submitProfile(){
    let obj={
      gender:"Male",
      name:this.name.value,
      specialization:this.specialization.value,
      city:this.city.value,
      access_token:localStorage.getItem('access_token'),
      user_id:Number(localStorage.getItem('user_id'))
    }
    this.apiService.buildDataProfile(obj).subscribe(data=>{
      console.log(data)
    })
  }

}
