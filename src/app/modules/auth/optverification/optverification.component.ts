import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import {ApiService} from "../../../services/api.service"
import { Router } from '@angular/router';

@Component({
  selector: 'app-optverification',
  templateUrl: './optverification.component.html',
  styleUrls: ['./optverification.component.css']
})
export class OptverificationComponent implements OnInit {
  otpForm:FormGroup
  submitted=false
  otp: AbstractControl;

  constructor(private formBuilder: FormBuilder,private apiService:ApiService,public router:Router) { }

  ngOnInit() {
    this.otp = new FormControl('', [Validators.required]);
    this.otpForm = this.formBuilder.group({
      otp: this.otp,
    })
  }

  get f() { 
    return this.otpForm.controls; }

  onSubmit(user) {
    this.submitted = true;
    console.log()
    // stop here if form is invalid
    if (this.otpForm.invalid) {
      return;
    }
  }

  submitOtp(){
    let obj={
      email: localStorage.getItem('email'),
       otp:this.otp.value,
       access_token:localStorage.getItem('access_token') 
    }
    this.apiService.verifyUser1(obj).subscribe(data=>{
    
    })
  }

}
