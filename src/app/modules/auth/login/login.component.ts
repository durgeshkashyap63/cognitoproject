import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import {Auth } from 'aws-amplify';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup
  email: AbstractControl;
  password:AbstractControl;
  constructor(private formBuilder: FormBuilder,private router:Router) { }

  ngOnInit() {
    this.email = new FormControl('', [Validators.required, Validators.email]);
    this.password = new FormControl('', [Validators.required]);
    this.loginForm = this.formBuilder.group({
      email: this.email,
      password:this.password
    })
  }
  async loginViaCognito() {
    try {
      var user = await Auth.signIn(this.email.toString(), this.password.toString());
      var token = user.signInUserSession;
      if (token != null) {
        this.router.navigate(['home']);
        alert("LoggedIn successfully");
      }
    }
    catch (e) {
      alert("Authentication Failed");
    }
  }

}
