import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import {ApiService} from '../../../services/api.service';
import { Router } from '@angular/router';
import {Auth } from 'aws-amplify';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  signupForm:FormGroup
  email: any;
  password:any;
  fullName:any;
  phoneNumber:any;
  countryCode:any
  submitted=false;
  constructor(private formBuilder: FormBuilder,private apiService:ApiService,public router:Router ) { }

  ngOnInit() {
    this.email = new FormControl('', [Validators.required, Validators.email]);
    this.password = new FormControl('', [Validators.required]);
    this.fullName = new FormControl('', [Validators.required]);
    this.phoneNumber = new FormControl('', [Validators.required]);
    this.countryCode = new FormControl('', [Validators.required]);
    this.signupForm = this.formBuilder.group({
      email: this.email,
      fullName:this.fullName,
      password:this.password,
      phoneNumber:this.phoneNumber,
      countryCode:this.countryCode
    })
  }

  onSub(){
let obj={
  fullName:this.fullName.value,
  email:this.email.value,
  password:this.password.value,
  phoneNumber:this.countryCode.value+this.phoneNumber.value,
  country_code:this.countryCode.value

}
    this.apiService.signUp(obj).subscribe(
      data=>{

        if(data && data.status==200){
          console.log(data.data.access_token)
          localStorage.setItem('access_token',data.data.access_token)
          localStorage.setItem('email',data.data.email)
          localStorage.setItem('fullName',data.data.fullName)
          localStorage.setItem('user_id',data.data.user_id)
         let  sendOtpData={
           access_token:localStorage.getItem('access_token'),
          // phoneNumber:this.countryCode.value+this.phoneNumber.value,
          email:this.email.value
         }
          this.apiService.sendOtp(sendOtpData).subscribe(data=>{
            if(data && data.status==200){
              this.router.navigate(['/auth/otpverification'])
            }
          })
        }
      }
    )

    }


  get f() {
    return this.signupForm.controls; }

  onSubmit(user) {
    this.submitted = true;
    console.log()
    // stop here if form is invalid
    if (this.signupForm.invalid) {
      return;
    }
  }


  signupViaCoginto() {
    try {
      let user = Auth.signUp({
        username: this.fullName.value,
        password: this.password.value,
        attributes: {
          email: this.email.value
        }
      })
      this.router.navigate(['/login'])
      alert("Signup Completed");
    }
    catch(e){
      alert("Something Went Wrong!");
    }
  }
}
