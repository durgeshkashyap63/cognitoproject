import { Routes } from '@angular/router';
import {RegisterComponent} from './modules/auth/register/register.component'
export const AppRoutes: Routes = [
  {
    path: 'sign-up',
    pathMatch: 'full',
    component: RegisterComponent,
    redirectTo: 'signup'
  },
];
