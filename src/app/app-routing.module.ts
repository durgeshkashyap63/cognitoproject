import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // { path: 'Auth', loadChildren: './modules/auth/auth.module#AuthModule' },
  // {
  //   path:'',
  //   pathMatch:'full',
  //   redirectTo:'panel'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
